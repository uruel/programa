<?php


namespace App\Tests\src\Service\DebounceIO;


use App\Service\DebounceIO\ValidationStatus;
use PHPUnit\Framework\TestCase;
use Webmozart\Assert\InvalidArgumentException;

class ValidationStatusTest extends TestCase
{

    /**
     * @dataProvider correctResponsesFromDebounceDisposableApi
     */
    public function testDisposabilityWithGoodResponse($jsonString, $expectedResult)
    {
        $data = json_decode($jsonString, true, 2);
        $validationStatus = new ValidationStatus($data);
        $this->assertEquals($expectedResult, $validationStatus->isDisposable());
    }

    /**
     * @dataProvider malformedResponsesFromDebounceDisposableApi
     */
    public function testDisposabilityWithMalformedResponse($jsonString, $expectedResult)
    {
        $this->expectException(InvalidArgumentException::class);
        $data = json_decode($jsonString, true, 2);
        $validationStatus = new ValidationStatus($data);
    }

    public function correctResponsesFromDebounceDisposableApi()
    {
        return [
            'Email is temporary' => ['{"disposable":"true"}', true],
            'Email is NOT temporary' => ['{"disposable":"false"}', false]
        ];
    }

    public function malformedResponsesFromDebounceDisposableApi()
    {
        return [
            'No disposable key' => ['{"something":"true"}', false],
            'String instead of boolean' => ['{"disposable":"maybe"}', false],
            'Integer instead of boolean' => ['{"disposable":"0"}', false],
            'Array instead of boolean' => ['{"disposable":"[]"}', false],
            'Object instead of boolean' => ['{"disposable":"[]"}', false],
        ];
    }

}