<?php


namespace App\Tests\src\Service\StringTransformation;


use App\Service\StringTransformation\LikeableOperator;
use PHPUnit\Framework\TestCase;

class LikeableOperatorTest extends TestCase
{
    /**
     * @dataProvider dataproviderWithSpaces
     * @dataProvider dataproviderWithoutSpaces
     */
    public function testPercentTokenInsersion($str, $expectedStr)
    {
        $transformation = new LikeableOperator();
        $transformedStr = $transformation->transform($str);
        $this->assertEquals($expectedStr, $transformedStr);
    }

    public function dataproviderWithSpaces()
    {
        return [
            'space at the begginning' => [' first', '%first%'],
            'double space' => ['first  second', '%first% %second%'],
            'triple space' => ['first   second', '%first% %second%'],
            'two double spaces, separated by word' => ['first  and  second', '%first% %and% %second%'],
        ];
    }

    public function dataproviderWithoutSpaces()
    {
        return [
            'empty string' => ['', ''],
            'No % in word' => ['this_is_some_string_to_transform', '%this_is_some_string_to_transform%'],
            '% in the middle of word' => ['some%partial_name', 'some%partial_name'],
            '% at the beggining of word' => ['%partial_name', '%partial_name'],
            '% at the end of word' => ['partial_name%', 'partial_name%'],
            'more then one % in single word' => ['some%partial%name', 'some%partial%name']
        ];
    }
}