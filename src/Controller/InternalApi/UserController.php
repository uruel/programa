<?php


namespace App\Controller\InternalApi;


use App\Service\DebounceIO\DisposableApiValidator;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/verify", methods={"POST"})
     */
    public function register(Request $request, DisposableApiValidator $disposableApiValidator): JsonResponse
    {
        $responseBody = $request->getContent();

        try {
            $data = json_decode($responseBody, true, 2, JSON_THROW_ON_ERROR);
            $isDisposableEmail = ($disposableApiValidator)($data['email']);
        } catch (JsonException $exception) {
            return new JsonResponse([
                'error' => 'Niepoprawne dane'
            ], Response::HTTP_NO_CONTENT);
        }

        if ($isDisposableEmail) {
            return new JsonResponse(['error' => [
                'validation' => [
                    'email' => 'Podany e-mail jest tymczasowy'
                ]
            ]], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'message' => 'Zapisano dane',
            'data' => $data
        ], Response::HTTP_CREATED);
    }
}