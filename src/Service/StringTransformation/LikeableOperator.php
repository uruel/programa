<?php


namespace App\Service\StringTransformation;


class LikeableOperator
{
    public function transform(string $str): string
    {
        $str = implode(' ', array_map(function ($str) {
            if (strpos($str, '%') === false) {
                return $str = '%' . $str . '%';
            }
            return $str;
        }, array_filter(explode(' ', $str))));
        return $str;
    }
}