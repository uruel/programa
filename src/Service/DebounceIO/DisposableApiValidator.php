<?php


namespace App\Service\DebounceIO;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use JsonException;
use function json_decode;

class DisposableApiValidator
{
    private string $debounceUri;
    /**
     * @var ClientInterface
     */
    private ClientInterface $client;

    public function __construct(string $debounceUri, ClientInterface $client)
    {
        $this->debounceUri = $debounceUri;
        $this->client = $client;
    }

    public function __invoke(string $email): bool
    {
        $httpResponse = $this->client->request('GET', $this->debounceUri, [
            'query' => ['email' => $email]
        ]);

        if($httpResponse->getStatusCode() < 200 && $httpResponse->getStatusCode() > 302) {
            return false;
        }

        $responseBody = $httpResponse->getBody()->__toString();
        try {
            $response = json_decode($responseBody, true, 2, JSON_THROW_ON_ERROR);
            $validationStatus = new ValidationStatus($response);
            return $validationStatus->isDisposable();
        } catch (JsonException $exception) {
            return false;
        }
    }

}