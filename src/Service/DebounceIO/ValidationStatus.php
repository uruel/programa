<?php


namespace App\Service\DebounceIO;


use Webmozart\Assert\Assert;

class ValidationStatus
{
    private bool $status;

    protected const DISPOSABLE_KEY = 'disposable';

    public function __construct(array $debounceApiResponse)
    {
        Assert::keyExists($debounceApiResponse, self::DISPOSABLE_KEY);
        Assert::inArray($debounceApiResponse[self::DISPOSABLE_KEY], ['true', 'false']);
        $this->status = $debounceApiResponse[self::DISPOSABLE_KEY] === "true";
    }

    public function isDisposable(): bool
    {
        return $this->status;
    }
}