<?php

namespace App\Repository;

use App\Entity\Product;
use App\Service\StringTransformation\LikeableOperator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var LikeableOperator
     */
    private $likeableOperatorTransformation;

    public function __construct(ManagerRegistry $registry, LikeableOperator $likeableOperatorTransformation)
    {
        parent::__construct($registry, Product::class);
        $this->likeableOperatorTransformation = $likeableOperatorTransformation;
    }

    public function getAvailibleProductsCount(): int
    {
        return $this->count(['availability' => true]);
    }

    /**
     * @return array<int, Product>
     */
    public function findUnavailibleProducts(): array
    {
        return $this->findBy(['availability' => false]) ;
    }

    /**
     * @return array<int, Product>
     */
    public function findNotAvailibleProducts(): array
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->where('p.availability = :availability')
            ->orWhere($qb->expr()->isNull('p.availability'))
            ->setParameter('availability', false)
            ->getQuery()->getResult();
    }
    /**
     * @return array<int, Product>
     */
    public function findProductsByNameLike(string $namePart): array
    {
        $qb = $this->createQueryBuilder('p');

        $namePart = $this->likeableOperatorTransformation->transform($namePart);

        if($namePart !== '') {
            $qb
                ->where($qb->expr()->like('p.name', ':name'))
                ->setParameter('name', $namePart);
        }

        return $qb
            ->getQuery()->getResult();
    }
}
