<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @return array<int, Product>
     */
    public function findProductsByCategory(Category $category): array
        //shouldn't this be in ProductRepository? all in all it returns Products, not Category
    {
        $categoryName = $category->getName();

        return $this->getEntityManager()->createQueryBuilder()
            ->from(Product::class, 'p')
            ->select('p')
            ->join('p.categories', 'c')
            ->where('c.name=:categoryName')
            ->setParameter('categoryName', $categoryName)
            ->getQuery()->getResult();
    }

    /**
     * @return array<int, Product>
     */
    public function findProductsByCategorySortedByName(Category $category): array
        //shouldn't this be in ProductRepository? all in all it returns Products, not Category
    {
        $categoryName = $category->getName();

        return $this->getEntityManager()->createQueryBuilder()
            ->from(Product::class, 'p')
            ->select('p')
            ->join('p.categories', 'c')
            ->where('c.name=:categoryName')
            ->orderBy('p.name', 'ASC')
            ->setParameter('categoryName', $categoryName)
            ->getQuery()->getResult();
    }
}
