#!/usr/bin/make

SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

down:
	docker-compose down

install_backend:
	docker-compose run --rm php composer install

install_frontend:
	docker-compose run --rm node yarn install

up:
	docker-compose up -d